var config      = require('./gulpfile.conf');
var gulp        = require('gulp');
var requireDir  = require('require-dir');
var dir         = requireDir('./tasks');

gulp.task('style',function()
{
    gulp.run('build:style');
    gulp.watch(['src/**/*.*'],function()
    {
          gulp.run('build:style');
    });
});

gulp.task('watch',function()
{
    gulp.run('build');
    gulp.watch(['src/**/*.*'],function()
    {
          gulp.run('build');
    });
});

gulp.task('default',function()
{
    gulp.run('watch');
});