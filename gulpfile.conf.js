module.exports = {
  /**
   * The `build_dir` folder is where our projects are compiled during
   * development and the `compile_dir` folder is where our app resides once it's
   * completely built.
   */
    buildDirectory : './build/',
    compileDirectory : './bin/',

  /**
   * This is a collection of file patterns that refer to our app code (the
   * stuff in `src/`). These file paths are used in the configuration of
   * build tasks. `js` is all project javascript, less tests. `ctpl` contains
   * our reusable components' (`src/common`) template HTML files, while
   * `atpl` contains the same, but for our app's code. `html` is just our
   * main HTML file, `less` is our main stylesheet, and `unit` contains our
   * app's unit tests.
   */
    appFiles : {
        script          :   [ './src/**/*.js', '!./src/**/*.spec.js', '!./src/asset/**/*.js' , '!./src/css/**/*.js'],
        test            :   [ './src/**/*.spec.js' ],
        template        :   [ './src/**/*.tpl.html' ],
        html            :   [ './src/index.html' ],
        asset           :   [ './src/asset/**/*.*' ],
        css             :   [ './src/**/*.css', './src/css/main.less' ]
    },
    buildFiles:{
        script          :   'script',
        asset           :   'asset',
        css             :   'css',
        dependencies    :   'lib'
    },

    dependenciesOrder : [
        "**/jquery.js",
        "**/angular.js",
        "**/bootstrap.js"
    ]
};
