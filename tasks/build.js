var config          = require('../gulpfile.conf.js');
var gulp            = require('gulp');
var karma           = require('gulp-karma')
var clean           = require('gulp-clean');
var concat          = require('gulp-concat');
var notify          = require('gulp-notify');
var bowerFiles      = require('gulp-bower-files');
var useref          = require('gulp-useref')
var inject          = require('gulp-inject');
var _if             = require('gulp-if');
var ngHtml2Js       = require("gulp-ng-html2js");
var uglify          = require("gulp-uglify");
var minifyHtml      = require("gulp-minify-html");
var order           = require("gulp-order"); 
var less            = require('gulp-less');
var jshint          = require('gulp-jshint');
var runSequence     = require('run-sequence');
var eventStream     = require('event-stream');


var isWindows = /^win/.test(require('os').platform());


gulp.task('clean',function()
{
    return gulp.src('./build',{read: false})
            .pipe(clean());

});

gulp.task('test',function()
{
    return gulp.src('NULL')
            .pipe(
                karma({
                configFile:'karma.conf.js',
                action:'run'
                })
            );
});

gulp.task('build:style',function(callback)
{
    runSequence('clean',['build:move-dependencies','build:move-script','build:move-asset','build:move-css','build:move-template'],'build:add-path',
              callback);
});

gulp.task('build',function(callback)
{
    runSequence('test','clean',['build:move-dependencies','build:move-script','build:move-asset','build:move-css','build:move-template'],'build:add-path',
              callback);
});

gulp.task('build:move-template',function()
{
    return gulp.src(config.appFiles.template)
            // .pipe(minifyHtml({
            //     empty: true,
            //     spare: true,
            //     quotes: true
            // }))
            .pipe(ngHtml2Js({
                moduleName: "TemplatesApp"
            }))
            .pipe(concat("template.min.js"))
            // .pipe(uglify())
            .pipe(gulp.dest(config.buildDirectory+config.buildFiles.script));
});

gulp.task('build:move-dependencies',function()
{
    return bowerFiles().pipe(gulp.dest(config.buildDirectory+config.buildFiles.dependencies));  
})

gulp.task('build:move-script',function()
{
    return gulp.src(config.appFiles.script)
            .pipe(jshint())
            .pipe(jshint.reporter('jshint-stylish'))
            .pipe(gulp.dest(config.buildDirectory+config.buildFiles.script))
});

gulp.task('build:move-asset',function()
{
    return gulp.src(config.appFiles.asset)
            .pipe(gulp.dest(config.buildDirectory+config.buildFiles.asset))
});

gulp.task('build:move-css',function()
{
    return gulp.src(config.appFiles.css)
            .pipe(less())
            .pipe(gulp.dest(config.buildDirectory+config.buildFiles.css))
});

gulp.task('build:add-path',function()
{
    var injectFiles = [
        config.buildDirectory+config.buildFiles.dependencies+'/**/*.js',
        config.buildDirectory+config.buildFiles.dependencies+'/**/*.css',
        config.buildDirectory+config.buildFiles.script+'/**/*.js',
        config.buildDirectory+config.buildFiles.css+'/**/*.css',
    ];
    
    return gulp.src(config.appFiles.html)
            .pipe(
                inject( gulp.src(injectFiles, {read: false}).pipe(order(config.dependenciesOrder)),{addRootSlash:false,ignorePath: '/build/'})
            )
            .pipe(gulp.dest('./build'))
            .pipe(_if(!isWindows,notify({message:'Build Success'})));
});

