describe( 'Order Profile Directive', function() {
    var $scope,element;

    describe( 'Positive Testing', function() {
        beforeEach(module('TemplatesApp'));
        beforeEach(module('orderProfile'));
        beforeEach(inject( function($compile, $rootScope) {
           $scope   = $rootScope;
           element  = angular.element("<div order-profile></div>");
           $compile(element)($scope);
           $scope.$digest();
        }));
        it( 'title should be haha',function(){
            expect($scope.title).toBe('haha');
        });
        it( 'title should be haha',function(){
            expect(element.find('div').hasClass('hello-world')).toBe(true);
        });
    });
    describe( 'Negative Testing', function() {

    });
});
