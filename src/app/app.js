angular.module( 'SWU', [
    'TemplatesApp',
    'orderProfile',
    'SWU.Home',
    'SWU.Login',
    'ui.router'
])
.config( function myAppConfig ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/home');
})
.run( function run () {

})
.controller( 'AppCtrl', function OrderCtrl( $scope ) {
    $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
        if ( angular.isDefined( toState.data.pageTitle ) ) {
            $scope.pageTitle = toState.data.pageTitle + ' | SWU Language Center' ;
        }
    });
});