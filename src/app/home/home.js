angular.module('SWU.Home',[
    'ui.router',
    'Model',
    'ngCsv'
])
.config(function($stateProvider,$locationProvider){
    $locationProvider.html5Mode(true);
    $stateProvider.state('home',{
        url:'/home',
        views:{
            main:{
                controller:'HomeController',
                templateUrl:'app/home/home.tpl.html'
            }
        },
        data:{
            pageTitle:'Home'
        }
    });
})
.run(['$rootScope', function ($rootScope) {

}])
.controller('HomeController',function($scope,schoolModel){


    $scope.popupFlag=false;
    $scope.topic = schoolModel.getTopic();
    $scope.schools= schoolModel.getSchools();
    // [
    //     {id:1 , name:"โรงเรียนจันทร์ประดิษฐารามวิทยาคม เขตภาษีเจริญ ",score:    93  }
    //     // {id:2 , name:"โรงเรียนแจงร้อนวิทยา เขตราษฎร์บูรณะ",score:    68  },
    //     // {id:3 , name:"โรงเรียนชิโนรสวิทยาลัย เขตบางกอกน้อย",score:    48  },
    //     // {id:4 , name:"โรงเรียนไชยฉิมพลีวิทยาคม เขตภาษีเจริญ",score:    65  },
    //     // {id:5 , name:"โรงเรียนเตรียมอุดมศึกษา เขตปทุมวัน",score:    86  },
    //     // {id:6 , name:"โรงเรียนไตรมิตรวิทยาลัย เขตสัมพันธวงศ์",score:    42  },
    //     // {id:7 , name:"โรงเรียนทวีธาภิเศก เขตบางกอกใหญ่",score:    59  },
    //     // {id:8 , name:"โรงเรียนทวีธาภิเศก 2 เขตบางขุนเทียน",score:    6   },
    //     // {id:9 , name:"โรงเรียนทีปังกรวิทยาพัฒน์ (ทวีวัฒนา) ในพระราชูปถัมภ์ฯ เขตทวีวัฒนา",score:    26  },
    //     // {id:10 , name:"โรงเรียนทีปังกรวิทยาพัฒน์ (วัดน้อยใน) ในพระราชูปถัมภ์ฯ เขตตลิ่งชัน",score:    56  },
    //     // {id:11 , name:"โรงเรียนเทพศิรินทร์ เขตป้อมปราบศัตรูพ่าย",score:    80  },
    //     // {id:12 , name:"โรงเรียนธนบุรีวรเทพีพลารักษ์ เขตธนบุรี",score:    19  },
    //     // {id:13 , name:"โรงเรียนนวมินทราชินูทิศ สตรีวิทยา พุทธมณฑล เขตทวีวัฒนา",score:    85  },
    //     // {id:14 , name:"โรงเรียนนวลนรดิศวิทยาคม รัชมังคลาภิเษก เขตบางบอน",score:    48  },
    //     // {id:15 , name:"โรงเรียนบางปะกอกวิทยาคม เขตราษฎร์บูรณะ",score:    65  },
    //     // {id:16 , name:"โรงเรียนบางมดวิทยา เขตจอมทอง",score:    7   },
    //     // {id:17 , name:"โรงเรียนเบญจมราชาลัย เขตพระนคร",score:    96  },
    //     // {id:18 , name:"โรงเรียนปัญญาวรคุณ เขตบางแค",score:    17  },
    //     // {id:19 , name:"โรงเรียนพิทยาลงกรณ์พิทยาคม เขตบางขุนเทียน",score:    29  },
    //     // {id:20 , name:"โรงเรียนโพธิสารพิทยากร เขตตลิ่งชัน",score:    12  },
    //     // {id:21 , name:"โรงเรียนมหรรณพาราม เขตตลิ่งชัน",score:    60  },
    //     // {id:22 , name:"โรงเรียนมักกะสันพิทยา เขตราชเทวี",score:    52  },
    //     // {id:23 , name:"โรงเรียนมัธยมวัดดาวคนอง เขตธนบุรี",score:    24  },
    //     // {id:24 , name:"โรงเรียนมัธยมวัดดุสิตาราม เขตบางกอกน้อย",score:    45  },
    //     // {id:25 , name:"โรงเรียนมัธยมวัดนายโรง เขตบางกอกน้อย",score:    38  },
    //     // {id:26 , name:"โรงเรียนมัธยมวัดเบญจมบพิตร เขตดุสิต",score:    95  },
    //     // {id:27 , name:"โรงเรียนมัธยมวัดมกุฏกษัตริย์ เขตพระนคร",score:    20  },
    //     // {id:28 , name:"โรงเรียนมัธยมวัดสิงห์ เขตจอมทอง",score:    58  },
    //     // {id:29 , name:"โรงเรียนมัธยมวัดหนองแขม เขตหนองแขม",score:    25  },
    //     // {id:30 , name:"โรงเรียนโยธินบูรณะ เขตดุสิต",score:    17  },
    //     // {id:31 , name:"โรงเรียนโยธินบูรณะ 2 (สุวรรณสุทธารามวิทยา) เขตบางซื่อ",score:    19  },
    //     // {id:32 , name:"โรงเรียนรัตนโกสินทร์สมโภชบางขุนเทียน เขตบางขุนเทียน",score:    15  },
    //     // {id:33 , name:"โรงเรียนราชนันทาจารย์ สามเสนวิทยาลัย ๒ เขตบางซื่อ",score:    15  },
    //     // {id:34 , name:"โรงเรียนราชวินิตบางแคปานขำ เขตบางแค",score:    30  }
    // ];

    $scope.addNewSchool = function(){
        if($scope.newSchoolName)
            $scope.schools.push({id:$scope.schools.length+1, name:$scope.newSchoolName, score:0})
        $scope.newSchoolName='';

        schoolModel.setSchools($scope.schools);
    };
    $scope.reset = function(){
        if(confirm('ท่านต้องการทำให้ข้อมูลทั้งหมดจะหายไป ใช่หรือไม่')==true)
        {
            schoolModel.clearSchools();    
            $scope.schools= schoolModel.getSchools();
        }
       
    }

    $scope.$watchCollection('[topic.name,topic.round]',function(newValue,oldValue){
        if(newValue==oldValue) return;
        schoolModel.setTopic($scope.topic);
    })
})
.directive('typeSearch',function($document,$window){
    return {
        restrict:'A',
        scope:{                
            search:'=typeSearch'
        },
        link:function(scope,element,attrs,controller){
            if(!controller.isShow){
                scope.search = "";
                $document.bind("keypress", function (event) {
                    event.preventDefault();
                    event.stopPropagation();

                    if(event.keyCode == 8)
                    {
                        scope.search = scope.search.slice(0, - 1)
                        scope.$apply();
                    }
                    else{
                        var character = String.fromCharCode(event.which);
                        scope.search = scope.search+character;
                        scope.$apply();
                    }
                    
                });

                scope.$watch('search',function(newValue,oldValue){
                    if(newValue == oldValue) return;

                    if(!newValue){
                        element.removeClass('on-search');
                        element.find('.search-container').css('display','none').animate({
                            opacity: 0.0
                        }, 600);
                        controller.isShow = false;
                    }
                    else if(!element.hasClass('on-search')){

                        element.addClass('on-search');
                        element.find('.search-container').css('display','block').animate({
                            opacity: 1.0
                        }, 600);
                        controller.isShow = true;
                    }
                });
            }
        },
        controller:function($scope){

            this.isShow = false;
        },
        replace: true
    };
})
.directive('listBox',function($compile){
    return {
        restrict:'A',
        templateUrl:'app/home/listBox.tpl.html',
        scope:{                
            schoolData:'=listBox',
            schools:'=schoolList',
            index:'=schoolIndex'
        },
        link:function(scope,element,attrs,controller){
            scope.$watch('schoolData.edit',function(newValue,oldValue){
                if(newValue == oldValue) return;
                if(newValue == true)
                {
                    element.find('.school-name').html('<div><input type="text" class="school-name-field"ng-model="schoolData.name"></div>');
                    $compile(element.find('.school-name').contents())(scope)

                    element.find('.school-name-field').on('keypress',function(event){
                        event.stopPropagation();
                    });
                }
                else{
                     element.find('.school-name').remove()
                     element.find('.detail').append('<div class="school-name">{{schoolData.name}}</div>');
                     $compile( element.find('.school-name'))(scope);
                }
            });
        },
        controller:function($scope,schoolModel){
            $scope.upScore = function(){
                $scope.schoolData.score++;
                schoolModel.setSchools($scope.schools);
            }
            $scope.downScore = function(){
                if($scope.schoolData.score-1 >= 0)
                    $scope.schoolData.score--;
                schoolModel.setSchools($scope.schools);
            }
            $scope.edit = function(){
                $scope.schoolData.edit = true;
            }
            $scope.save = function(){
                $scope.schoolData.edit = false;
                schoolModel.setSchools($scope.schools);
            }
            $scope.remove = function(){
                $scope.schoolData.edit = false;
                $scope.schools.splice($scope.index,1);
                schoolModel.setSchools($scope.schools);
            }
           
        },
        replace: true
    };
})
.directive('popup',function(){
    return {
        restrict:'A',
        scope:{                
            popupFlag:'=popup'
        },
        link:function(scope, element, attrs) {

            scope.$watch('popupFlag',function(){
                if(scope.popupFlag===true)
                    element.css('display','block');
                else
                    element.css('display','none');
            });

        }
    };
});