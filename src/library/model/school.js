angular.module('Model',['LocalStorageModule'])
.factory('schoolModel',function($http,localStorageService)
{
    return{
        
        setTopic:function(topic){
            localStorageService.set('topic',topic);
        },
        getTopic:function(){
            if(localStorageService.get('topic'))
                return localStorageService.get('topic');
            else
                return {
                    name:'ยังไม่ได้ตั้งหัวข้อ',
                    round:'0'
                };
        },
        clearTopic:function(){
           localStorageService.remove('topic');
        },
        setSchools:function(schools){
            localStorageService.set('schools',schools);
        },
        getSchools:function(){
            if(localStorageService.get('schools'))
                return localStorageService.get('schools');
            else
                return [];
        },
        clearSchools:function(){
           localStorageService.remove('schools');
        }
    };
});